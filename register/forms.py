from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):
    email = forms.EmailField()
    nama_lengkap = forms.CharField(max_length=50)

    class Meta:
        model = User
        fields = ['username', 'email', 'nama_lengkap', 'password1', 'password2']

class LoginForm():
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password']
