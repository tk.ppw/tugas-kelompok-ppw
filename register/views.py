from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login
from .forms import LoginForm, RegisterForm

# def login(request):
#     if request.method == 'POST':
#         form = LoginForm(request.POST or None)
#         if form.is_valid():
#             username = request.POST['username']
#             password = request.POST['password']
#             user = authenticate(username=username, password=password)
#             if user is not None : 
#                 login(request, user)
#                 return redirect('landing')

#     else:
#         form = LoginForm()
#     return render(request,'registration/login.html', {'form':form})

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            user = form.save(commit=False)
            user.save()
            messages.success(request, f'Welcome {username}, your account is created')
            
            return redirect('landing')
    else:
        form = RegisterForm()
    return render(request, 'register/register.html', {'form':form})