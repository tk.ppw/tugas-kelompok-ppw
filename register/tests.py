from django.test import TestCase
from django.test import TestCase, Client
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC


class UnitTest(TestCase):

    def test_register_using_the_right_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register/register.html')

    def test_post_failure_register(self):
        response = Client().post('/register/')
        self.assertEqual(response.status_code, 200)


# class SeleniumRegisterTestCase(LiveServerTestCase):

#     def setUp(self):
#         # ChromeDriver 81.0.4044.69
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome(
#             './chromedriver', chrome_options=chrome_options)
#         super(SeleniumRegisterTestCase, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(SeleniumRegisterTestCase, self).tearDown()

#     def RegisterTestCase(self):
#         driver = self.selenium
#         driver.get(self.live_server_url + '/register/')

#         username1 = driver.find_element_by_xpath("//input[@name='username']")
#         username1.send_keys('user001')

#         email1 = driver.find_element_by_xpath("//input[@name='email']")
#         email1.send_keys('user001@abc.com')

#         password1 = driver.find_element_by_xpath("//input[@name='password1']")
#         password1.send_keys('nyobanyoba')

#         password2 = driver.find_element_by_xpath("//input[@name='password2']")
#         password2.send_keys('nyobanyoba')

#         register = driver.find_element_by_tag_name('button')
#         register.click()

#         tombolpanah = driver.find_element_by_id('navbarDropdownMenuLink')
#         tombolpanah.click()

#         login = driver.find_element_by_name('login')
#         login.click()

#         username2 = driver.find_element_by_xpath("//input[@name='username']")
#         username2.send_keys('user001')

#         password = driver.find_element_by_xpath("//input[@name='password']")
#         password.send_keys('nyobanyoba')

#         login_button = driver.find_element_by_tag_name('button')
        # login_button.click()

        # assert 'user001' in driver.page_source
