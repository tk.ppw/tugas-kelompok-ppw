from django.urls import path
from .views import barang_view, semua_barang_view, barang_view_buy, barang_view_review

app_name = 'barang'

urlpatterns = [
    path('<int:id>/buy', barang_view_buy, name='barang-buy'),
    path('<int:id>/review', barang_view_review, name='barang-review'),
    path('<int:id>', barang_view, name='barang-detail'),
    path('all/<str:filter>', semua_barang_view, name='barang-filter'),
    path('all', semua_barang_view, name='barang-all')
]
