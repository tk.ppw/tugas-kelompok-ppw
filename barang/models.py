from django.db import models
from kategori.models import Kategori


class Barang(models.Model):
    nama = models.CharField(max_length=80)
    harga = models.DecimalField(decimal_places=2, max_digits=30)
    stok = models.PositiveSmallIntegerField()
    deskripsi = models.TextField()
    kategori = models.ForeignKey(
        Kategori, on_delete=models.SET_NULL, related_name="kategori", null=True, blank=True)
    gambar = models.URLField(
        default="https://media.swansonvitamins.com/images/items/master/SW1371.jpg")
