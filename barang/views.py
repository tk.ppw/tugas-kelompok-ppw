from django.shortcuts import render
from django.db.models import Avg
from .models import Barang
from transaksi.models import Transaksi
from ulasan.models import Ulasan
from transaksi.forms import BeliBarangForm
from ulasan.forms import UlasanForm


def barang_view_buy(request, id):
    if request.method == "POST":
        barang_requested = Barang.objects.get(pk=id)

        beli_form = BeliBarangForm(barang_requested.stok, request.POST)

        success = False
        action = "Transaction"

        if beli_form.is_valid():
            trx = Transaksi()
            trx.nama_pembeli = beli_form.cleaned_data["nama_pembeli"]
            trx.barang = barang_requested
            trx.kupon = beli_form.cleaned_data["kupon"]
            trx.total = barang_requested.harga * \
                beli_form.cleaned_data["jumlah"]
            if trx.kupon:
                if trx.total > trx.kupon.min_harga:
                    trx.total = trx.total * \
                        (100 - trx.kupon.persen_diskon) / 100
            trx.save()

            # decrease item stock
            barang_requested.stok -= beli_form.cleaned_data["jumlah"]
            barang_requested.save()

            # reset form
            beli_form = BeliBarangForm(barang_requested.stok)
            success = True

        ctx_override = {
            "beli_form": beli_form,
            "success": success,
            "action": action
        }

        return barang_view(request, id, ctx_override=ctx_override)
    else:
        return barang_view(request, id)


def barang_view_review(request, id):
    if request.method == "POST":
        barang_requested = Barang.objects.get(pk=id)

        ulasan_form = UlasanForm(request.POST)

        success = False
        action = "Review"

        if ulasan_form.is_valid():
            ulasan_form.save()

            # reset form
            ulasan_form = UlasanForm(initial={"barang": barang_requested})
            success = True

        ctx_override = {
            "ulasan_form": ulasan_form,
            "success": success,
            "action": action
        }

        return barang_view(request, id, ctx_override=ctx_override)
    else:
        return barang_view(request, id)


def barang_view(request, id, ctx_override={}):
    barang_requested = Barang.objects.get(pk=id)

    beli_form = BeliBarangForm(stok=barang_requested.stok)
    ulasan_form = UlasanForm(
        initial={"barang": barang_requested, "nama_pengulas": request.user.username})

    ulasan_barang = Ulasan.objects.filter(barang=barang_requested)
    avg_stars = ulasan_barang.aggregate(Avg("bintang"))

    context = {
        "barang": barang_requested,
        "beli_form": beli_form,
        "avg_rating": avg_stars["bintang__avg"] if avg_stars["bintang__avg"] else 0,
        "num_rating": ulasan_barang.count(),
        "ulasan_barang": ulasan_barang,
        "ulasan_form": ulasan_form
    }

    for k, v in ctx_override.items():
        context[k] = v

    return render(request, "barang/barang.html", context)


def semua_barang_view(request, filter=""):
    filter = filter if filter else request.GET.get("filter", "")
    semua_barang = Barang.objects.filter(nama__contains=filter)
    context = {
        "semua_barang": semua_barang,
        "filter": filter if filter else None
    }
    return render(request, "barang/semua.html", context)
