from django.test import TestCase, override_settings
from django.test import Client
from django.urls import resolve
from .views import barang_view, barang_view_buy, barang_view_review, semua_barang_view
from .models import Barang
from kategori.models import Kategori


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class barangUnitTest(TestCase):
    def setUp(self):
        test_kategori = Kategori.objects.create(nama='Test')
        self.new_barang = Barang.objects.create(
            nama='Test', harga=10000, stok=100, deskripsi='Haha', kategori=test_kategori)

    def test_barang_url_is_exist(self):
        response = Client().get('/barang/1')
        self.assertEqual(response.status_code, 200)

    def test_barang_using_view_func(self):
        found = resolve('/barang/1')
        self.assertEqual(found.func, barang_view)

    def test_barang_all_url_is_exist(self):
        response = Client().get('/barang/all')
        self.assertEqual(response.status_code, 200)

    def test_barang_all_using_view_func(self):
        found = resolve('/barang/all')
        self.assertEqual(found.func, semua_barang_view)

    def test_model_can_create_new_barang(self):
        # Retrieving all available activity
        counting_barang = Barang.objects.all().count()
        self.assertEqual(counting_barang, 1)

    def test_barang_buy_post_success_and_render_the_result(self):
        test = 'Transaction Saved'
        response_post = Client().post(
            '/barang/1/buy', {'nama_pembeli': 'Haha', 'jumlah': 1})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_barang_review_post_success_and_render_the_result(self):
        test = 'Review Saved'
        response_post = Client().post(
            '/barang/1/review', {'nama_pengulas': 'Haha', 'bintang': 1, 'pesan': 'Haha', 'barang': self.new_barang.id})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_barang_review_post_error_and_render_the_result(self):
        test = 'is-invalid'
        response_post = Client().post(
            '/barang/1/review', {'nama_pembeli': '', 'jumlah': 9999999})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertIn(test, html_response)
