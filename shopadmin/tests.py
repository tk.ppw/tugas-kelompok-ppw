from django.test import TestCase, override_settings
from django.urls import reverse
from django.test import Client
from django.urls import resolve
from django.contrib.auth.models import User


from .views import add_view
from kategori.models import Kategori


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class barangUnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_superuser(
            'john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')

        self.test_kategori = Kategori.objects.create(nama='Test')

    def test_logged_in_admin(self):
        response = self.client.get(reverse('shopadmin:shop-admin'))
        self.assertEqual(response.status_code, 200)

    def test_admin_using_view_func(self):
        found = resolve('/shopadmin/add/barang')
        self.assertEqual(found.func, add_view)

    def test_barang_admin_url_is_exist(self):
        response = self.client.get('/shopadmin/add/barang')
        self.assertEqual(response.status_code, 200)

    def test_kategori_admin_url_is_exist(self):
        response = self.client.get('/shopadmin/add/kategori')
        self.assertEqual(response.status_code, 200)

    def test_kupon_admin_url_is_exist(self):
        response = self.client.get('/shopadmin/add/kupon')
        self.assertEqual(response.status_code, 200)

    def test_admin_post_error(self):
        test = 'is-invalid'
        response_post = self.client.post(
            '/shopadmin/add/kupon', {'nama_pembeli': '', 'jumlah': 9999999})
        self.assertEqual(response_post.status_code, 200)

        html_response = response_post.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_barang_admin_post_success_and_render_the_result(self):
        response_post = self.client.post(
            '/shopadmin/add/barang', {'nama': 'Haha', 'stok': 1, 'deskripsi': 'Haha', 'harga': 100000, 'kategori': 'Test', 'gambar': 'http://google.com'})
        self.assertEqual(response_post.status_code, 302)

    def test_kategori_admin_post_success_and_render_the_result(self):
        response_post = self.client.post(
            '/shopadmin/add/kategori', {'nama': 'Haha'})
        self.assertEqual(response_post.status_code, 302)

    def test_kupon_admin_post_success_and_render_the_result(self):
        response_post = self.client.post(
            '/shopadmin/add/kupon', {'kode': 'Haha', 'persen_diskon': 20, 'min_harga': 0, 'expired': '2021-04-24T19:29'})
        self.assertEqual(response_post.status_code, 302)
