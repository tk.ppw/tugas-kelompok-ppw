from django import forms

from barang.models import Barang
from kupon.models import Kupon
from kategori.models import Kategori


class BarangCreateForm(forms.ModelForm):
    class Meta:
        model = Barang
        fields = "__all__"


class KuponCreateForm(forms.ModelForm):
    expired = forms.DateTimeField(
        input_formats=['%Y-%m-%dT%H:%M'],
        widget=forms.TextInput(
            attrs={'type': 'datetime-local'}
        )
    )

    class Meta:
        model = Kupon
        fields = "__all__"


class KategoriCreateForm(forms.ModelForm):
    class Meta:
        model = Kategori
        fields = "__all__"
