from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render, redirect

from .forms import KategoriCreateForm, BarangCreateForm, KuponCreateForm


@staff_member_required
def admin_view(request):
    return render(request, "shopadmin/admin.html")


def handle_post_add(request, name):
    if name.lower() == "kupon":
        form = KuponCreateForm(request.POST)
        name = "Kupon"
    elif name.lower() == "kategori":
        form = KategoriCreateForm(request.POST)
        name = "Kategori"
    elif name.lower() == "barang":
        form = BarangCreateForm(request.POST)
        name = "Barang"
    if form.is_valid():
        form.save()
        return redirect("kupon:semua-kupon")
    context = {
        "form": form,
        "name": name
    }
    return render(request, "shopadmin/add.html", context)


@staff_member_required
def add_view(request, name):
    if request.method == "POST":
        return handle_post_add(request, name)
    if name == "kupon":
        kupon_form = KuponCreateForm()
        context = {
            "form": kupon_form,
            "name": "Kupon"
        }
    elif name == "kategori":
        kategori_form = KategoriCreateForm()
        context = {
            "form": kategori_form,
            "name": "Kategori"
        }
    elif name == "barang":
        barang_form = BarangCreateForm()
        context = {
            "form": barang_form,
            "name": "Barang"

        }
    else:
        return render(request, "shopadmin/admin.html")
    return render(request, "shopadmin/add.html", context)
