# Tugas Kelompok PPW TK.PePeW

Tugas Kelompok 1 PPW dari kelompok **TK.PePeW** dengan mengambil studi kasus _'PW Online Shop'_.

Tugas ini telah terdeploy di Heroku di URL [ini](https://tk-ppw.herokuapp.com/).

[![pipeline status](https://gitlab.com/tk.ppw/tugas-kelompok-ppw/badges/master/pipeline.svg)](https://gitlab.com/tk.ppw/tugas-kelompok-ppw)

[![coverage report](https://gitlab.com/tk.ppw/tugas-kelompok-ppw/badges/master/coverage.svg)](https://gitlab.com/tk.ppw/tugas-kelompok-ppw)

## Anggota

- Eko Julianto Salim (1906350925)
- Steven Wiryadinata Halim (1906350622)
- Gita Permatasari Sujatmiko (1906400053)
- Rafli Bangsawan (1906398414)
- Mauludi Afina Mirza (1606884836)
