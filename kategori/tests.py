from django.test import TestCase, override_settings
from django.test import Client
from django.urls import resolve
from .views import kategori_view
from .models import Kategori
from barang.models import Barang


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class barangUnitTest(TestCase):
    def setUp(self):
        test_kategori = Kategori.objects.create(nama='Test')
        self.new_barang = Barang.objects.create(
            nama='Test', harga=10000, stok=100, deskripsi='Haha', kategori=test_kategori)

    def test_kategori_url_is_exist(self):
        response = Client().get('/kategori/')
        self.assertEqual(response.status_code, 200)

    def test_barang_using_view_func(self):
        found = resolve('/kategori/')
        self.assertEqual(found.func, kategori_view)
