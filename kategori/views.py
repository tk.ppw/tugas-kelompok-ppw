from django.shortcuts import render
from .models import Kategori
from barang.models import Barang


def kategori_view(request, kategori=""):
    kategori_all = Kategori.objects.all()
    try:
        kategori_requested = kategori_all.get(nama=kategori)
    except:
        kategori_requested = kategori_all[0]
    barang_requested = Barang.objects.filter(kategori=kategori_requested)
    context = {
        "categories": kategori_all,
        "prod": barang_requested,
        "cat": kategori_requested
    }
    return render(request, "kategori/kategori.html", context)
