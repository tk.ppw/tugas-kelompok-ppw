from django.urls import path
from .views import keranjang_view, keranjang_add_view, keranjang_update_jumlah_view, keranjang_hapus_view

app_name = 'keranjang'

urlpatterns = [
    path('', keranjang_view, name='keranjang'),
    path('add', keranjang_add_view, name='add'),
    path('update-jumlah', keranjang_update_jumlah_view, name='update-jumlah'),
    path('hapus', keranjang_hapus_view, name='hapus')
]
