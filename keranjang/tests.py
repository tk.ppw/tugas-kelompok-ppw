from django.test import TestCase, override_settings
from django.test import Client
from django.urls import resolve
from django.contrib.auth.models import User

from .views import keranjang_view, keranjang_add_view, keranjang_hapus_view, keranjang_update_jumlah_view

from kategori.models import Kategori
from barang.models import Barang


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class keranjangUnitTest(TestCase):
    def setUp(self):
        test_kategori = Kategori.objects.create(nama='Test')
        self.new_barang = Barang.objects.create(
            nama='Test', harga=10000, stok=100, deskripsi='Haha', kategori=test_kategori)

        self.client = Client()
        self.user = User.objects.create_user(
            'john', 'lennon@thebeatles.com', 'johnpassword')

        self.client.login(username='john', password='johnpassword')

    def test_keranjang_url_is_exist(self):
        response = self.client.get('/keranjang/')
        self.assertEqual(response.status_code, 200)

    def test_keranjang_using_view_func(self):
        found = resolve('/keranjang/')
        self.assertEqual(found.func, keranjang_view)

    def test_keranjang_add_using_view_func(self):
        found = resolve('/keranjang/add')
        self.assertEqual(found.func, keranjang_add_view)

    def test_keranjang_hapus_using_view_func(self):
        found = resolve('/keranjang/hapus')
        self.assertEqual(found.func, keranjang_hapus_view)

    def test_keranjang_jumlah_using_view_func(self):
        found = resolve('/keranjang/update-jumlah')
        self.assertEqual(found.func, keranjang_update_jumlah_view)

    def test_post_failure_keranjang_add(self):
        response = self.client.post('/keranjang/add')
        self.assertEqual(response.status_code, 200)

    def test_post_failure_keranjang_hapus(self):
        response = self.client.post('/keranjang/hapus')
        self.assertEqual(response.status_code, 200)

    def test_post_failure_keranjang_jumlah(self):
        response = self.client.post('/keranjang/update-jumlah')
        self.assertEqual(response.status_code, 200)
