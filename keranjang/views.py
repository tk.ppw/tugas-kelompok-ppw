import json
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.forms.models import model_to_dict
from django.http import JsonResponse

from tk2.models import KeranjangBarang, KeranjangBarangEntry
from barang.models import Barang


@login_required
def keranjang_view(request):
    try:
        keranjang_user = KeranjangBarang.objects.get(
            user=request.user, active=True)
    except:
        keranjang_user = None

    context = {
        "keranjang": keranjang_user
    }

    return render(request, "keranjang/keranjang.html", context)


@login_required
def keranjang_add_view(request):
    if request.method == "POST":
        try:
            keranjang_user, _ = KeranjangBarang.objects.get_or_create(
                user=request.user, active=True)

            data = json.loads(request.body)
            barang_user = Barang.objects.get(pk=data.get("barang_id"))

            if not KeranjangBarangEntry.objects.filter(barang=barang_user, keranjang=keranjang_user).exists():
                entry = KeranjangBarangEntry(
                    barang=barang_user, jumlah=1, keranjang=keranjang_user)
                entry.save()
            else:
                raise Exception("Barang sudah ada di keranjang!")

            return JsonResponse({'success': True, 'entry': model_to_dict(entry)})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})


@login_required
def keranjang_update_jumlah_view(request):
    if request.method == "POST":
        try:
            keranjang_user, _ = KeranjangBarang.objects.get_or_create(
                user=request.user, active=True)

            data = json.loads(request.body)

            barang_user = KeranjangBarangEntry.objects.get(pk=data.get("id"))
            if int(data.get("jumlah")) <= barang_user.barang.stok:
                barang_user.jumlah = int(data.get("jumlah"))
                barang_user.save()
            else:
                raise Exception("Jumlah lebih besar dari stok!")
            return JsonResponse({'success': True, 'entry': model_to_dict(barang_user)})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})


@login_required
def keranjang_hapus_view(request):
    if request.method == "POST":
        try:
            keranjang_user, _ = KeranjangBarang.objects.get_or_create(
                user=request.user, active=True)

            data = json.loads(request.body)

            barang_user = KeranjangBarangEntry.objects.get(pk=data.get("id"))
            barang_user.delete()

            return JsonResponse({'success': True, 'keranjang': model_to_dict(keranjang_user)})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})
