from django.test import TestCase, override_settings
from django.test import Client
from django.urls import resolve
from .views import all_transaction_view, cepat_view, riwayat_cepat_view
from .models import Transaksi
from kategori.models import Kategori
from barang.models import Barang


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class transaksiUnitTest(TestCase):
    def setUp(self):
        test_kategori = Kategori.objects.create(nama='Test')
        self.new_barang = Barang.objects.create(
            nama='Test', harga=10000, stok=100, deskripsi='Haha', kategori=test_kategori)
        new_transaksi = Transaksi.objects.create(
            nama_pembeli='Test', barang=self.new_barang, total=10000)

    def test_transaksi_url_is_exist(self):
        response = Client().get('/transaksi/')
        self.assertEqual(response.status_code, 200)

    def test_transaksi_using_view_func(self):
        found = resolve('/transaksi/')
        self.assertEqual(found.func, all_transaction_view)

    def test_trxcepat_url_is_exist(self):
        response = Client().get('/transaksi/cepat/')
        self.assertEqual(response.status_code, 200)

    def test_trxcepat_using_view_func(self):
        found = resolve('/transaksi/cepat/')
        self.assertEqual(found.func, cepat_view)

    def test_riwayat_url_is_exist(self):
        response = Client().get('/transaksi/riwayat-cepat')
        self.assertEqual(response.status_code, 200)

    def test_riwayat_using_view_func(self):
        found = resolve('/transaksi/riwayat-cepat')
        self.assertEqual(found.func, riwayat_cepat_view)

    def test_post_failure_beli_cepat(self):
        response = self.client.post('/transaksi/api/beli-cepat')
        self.assertEqual(response.status_code, 200)

