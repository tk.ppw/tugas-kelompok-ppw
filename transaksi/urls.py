from django.urls import path
from .views import all_transaction_view, cepat_view, beli_cepat_view, riwayat_cepat_view

app_name = 'transaksi'

urlpatterns = [
    path('', all_transaction_view, name='all-transactions'),
    path('cepat/', cepat_view, name='transaksi-cepat'),
    path('riwayat-cepat', riwayat_cepat_view, name='riwayat-cepat'),
    path('api/beli-cepat', beli_cepat_view, name='beli-cepat')
]
