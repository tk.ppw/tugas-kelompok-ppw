from django.db import models
from barang.models import Barang
from kupon.models import Kupon


class Transaksi(models.Model):
    nama_pembeli = models.CharField(
        max_length=100, verbose_name="Nama Pembeli")
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    kupon = models.ForeignKey(
        Kupon, on_delete=models.SET_NULL, blank=True, null=True)
    total = models.DecimalField(max_digits=30, decimal_places=2)
    tanggal = models.DateTimeField(auto_now_add=True)
