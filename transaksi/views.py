import json
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from tk2.models import KeranjangBarang, KeranjangBarangEntry, KeranjangBarangTransaksi
from kupon.models import Kupon
from .models import Transaksi


def all_transaction_view(request):
    transaksi_all = Transaksi.objects.all().order_by("-tanggal")
    context = {
        "transaksi_all": transaksi_all
    }
    return render(request, "transaksi/semua.html", context)


def cepat_view(request):
    try:
        keranjang_user = KeranjangBarang.objects.get(
            user=request.user, active=True)
    except:
        keranjang_user = None

    context = {
        "keranjang": keranjang_user
    }

    return render(request, "transaksi/cepat.html", context)


def beli_cepat_view(request):
    if request.method == "POST":
        try:
            keranjang_user = KeranjangBarang.objects.get(
                user=request.user, active=True)

            data = json.loads(request.body)

            if KeranjangBarangEntry.objects.filter(keranjang=keranjang_user).exists():
                kupon = Kupon.objects.filter(id=data.get("kupon_id")).first()
                trx = KeranjangBarangTransaksi(
                    keranjang=keranjang_user, kupon=kupon, total_harga=data.get("total_harga"), user=request.user)
                trx.save()
                keranjang_user.active = False
                keranjang_user.save()

                # kurangi stok
                for b in keranjang_user.entry.all():
                    br = b.barang
                    br.stok -= b.jumlah
                    br.save()
            else:
                raise Exception("Tidak ada barang di keranjang!")

            return JsonResponse({'success': True})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})


def riwayat_cepat_view(request):
    try:
        transaksi_user = KeranjangBarangTransaksi.objects.filter(
            user=request.user)
    except:
        transaksi_user = None

    context = {
        "transaksi_all": transaksi_user
    }

    return render(request, "transaksi/riwayat.html", context)
