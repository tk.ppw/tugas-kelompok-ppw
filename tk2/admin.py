from django.contrib import admin

from .models import KeranjangBarang, KeranjangBarangEntry, KeranjangBarangTransaksi

admin.site.register(KeranjangBarang)
admin.site.register(KeranjangBarangEntry)
admin.site.register(KeranjangBarangTransaksi)
