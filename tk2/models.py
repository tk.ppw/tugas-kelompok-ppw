from django.db import models
from django.contrib.auth.models import User
from django.db.models import F, Sum, FloatField

from barang.models import Barang
from kupon.models import Kupon


class KeranjangBarang(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="keranjang")
    active = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now=True)

    @property
    def total(self):
        return self.entry.all().aggregate(total=Sum(F("barang__harga") * F("jumlah"), output_field=FloatField()))['total']


class KeranjangBarangEntry(models.Model):
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    jumlah = models.PositiveIntegerField()
    keranjang = models.ForeignKey(
        KeranjangBarang, on_delete=models.CASCADE, related_name="entry")

    @property
    def harga_total(self):
        return self.barang.harga * self.jumlah


class KeranjangBarangTransaksi(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="trxcepat", null=True)
    keranjang = models.ForeignKey(
        KeranjangBarang, on_delete=models.CASCADE, related_name="transaksi")
    kupon = models.ForeignKey(
        Kupon, on_delete=models.SET_NULL, blank=True, null=True)
    total_harga = models.PositiveIntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)
