from django.contrib import admin
from .models import Ulasan


class UlasanAdmin(admin.ModelAdmin):
    readonly_fields = ('tanggal',)


admin.site.register(Ulasan, UlasanAdmin)
