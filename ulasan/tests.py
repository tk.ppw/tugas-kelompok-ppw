from django.test import TestCase, override_settings
from django.test import Client
from django.urls import resolve
from .views import semua_ulasan_view
from kategori.models import Kategori
from barang.models import Barang
from .models import Ulasan


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class ulasanUnitTest(TestCase):
    def setUp(self):
        test_kategori = Kategori.objects.create(nama='Test')
        self.new_barang = Barang.objects.create(
            nama='Test', harga=10000, stok=100, deskripsi='Haha', kategori=test_kategori)
        self.new_ulasan = Ulasan.objects.create(
            nama_pengulas='Test', bintang=5, barang=self.new_barang, pesan='Haha')

    def test_ulasan_url_is_exist(self):
        response = Client().get('/ulasan/')
        self.assertEqual(response.status_code, 200)

    def test_ulasan_using_view_func(self):
        found = resolve('/ulasan/')
        self.assertEqual(found.func, semua_ulasan_view)
