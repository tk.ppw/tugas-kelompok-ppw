from django.urls import path
from .views import semua_ulasan_view, add_ulasan_view, delete_ulasan_view

app_name = 'ulasan'

urlpatterns = [
    path('', semua_ulasan_view, name='all-ulasan'),
    path('api/add', add_ulasan_view, name='add'),
    path('api/delete', delete_ulasan_view, name='delete')
]
