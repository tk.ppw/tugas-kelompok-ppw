import json
from django.shortcuts import render
from .models import Ulasan
from django.contrib.auth.decorators import login_required
from django.forms.models import model_to_dict
from django.http import JsonResponse

from barang.models import Barang


def semua_ulasan_view(request):
    ulasan_all = Ulasan.objects.all().order_by("-tanggal")
    context = {
        "ulasan_all": ulasan_all
    }
    return render(request, "ulasan/semua.html", context)


@login_required
def delete_ulasan_view(request):
    if request.method == "POST":
        try:
            data = json.loads(request.body)

            ulasan_user = Ulasan.objects.get(pk=data.get("ulasan_id"))
            ulasan_user.delete()

            semua_ulasan = Ulasan.objects.all().order_by("-tanggal")

            return JsonResponse({'success': True, 'ulasan': list(semua_ulasan.values())})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})


@login_required
def add_ulasan_view(request):
    if request.method == "POST":
        try:
            data = json.loads(request.body)

            barang_id = data.pop("barang_id")

            barang = Barang.objects.get(pk=barang_id)

            ulasan_user = Ulasan(barang=barang, **data)
            ulasan_user.save()

            semua_ulasan = Ulasan.objects.all().order_by("-tanggal")

            return JsonResponse({'success': True, 'ulasan': list(semua_ulasan.values())})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})
