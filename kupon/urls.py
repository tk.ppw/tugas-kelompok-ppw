from django.urls import path
from .views import semua_kupon_view, kupon_api_view

app_name = 'kupon'

urlpatterns = [
    path('', semua_kupon_view, name='semua-kupon'),
    path('api/semua', kupon_api_view, name='kupon-api')
]
