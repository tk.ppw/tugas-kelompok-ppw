from django.shortcuts import render
from .models import Kupon
import datetime
from django.http import JsonResponse


def semua_kupon_view(request):
    min_discount = request.GET.get("min-discount", 0)
    min_discount = min_discount if min_discount else 0
    coupon_filter = request.GET.get("coupon", "")
    kupon_all = Kupon.objects.filter(
        kode__contains=coupon_filter, persen_diskon__gte=min_discount)
    context = {
        "kupon_all": kupon_all,
        "min_discount": min_discount,
        "coupon_filter": coupon_filter
    }
    return render(request, "kupon/semua.html", context)

def kupon_api_view(request):
    time = datetime.datetime.now()
    kupons = Kupon.objects.filter(expired__gte = time)
    return JsonResponse({'kupons': list(kupons.values())})
