from django.test import TestCase, override_settings
from django.test import Client
from django.urls import resolve
from .views import semua_kupon_view


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class barangUnitTest(TestCase):
    def test_kategori_url_is_exist(self):
        response = Client().get('/kupon/')
        self.assertEqual(response.status_code, 200)

    def test_barang_using_view_func(self):
        found = resolve('/kupon/')
        self.assertEqual(found.func, semua_kupon_view)
